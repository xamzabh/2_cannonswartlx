﻿namespace CannonsWarTLX.Controller.States
{
    using System;
    using CannonsWarTLX.Model;
    using CannonsWarTLX.Model.Players;
    using CannonsWarTLX.View;
    using CannonsWarTLX.View.UI;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Input;

    public class PlayerTurnState : State
    {
        private const int SECOND_PLAYER_BUTTONS_X = 650;
        private const float ONE_DEGREE_IN_RADIANS = 0.01745329252f;

        public PlayerTurnState(State nextState)
            : base(nextState)
        {
        }

        public override void Execute()
        {
            SetPlayerData();

            KeyboardState keyboard = EntryPoint.Game.KeyboardState;
            KeyboardState oldKeyboard = EntryPoint.Game.OldKeyboardState;

            EntryPoint.Game.Renderer.DrawGameEnvironment();

            this.SpeedButtonHandler(PlayfieldModel.Players[0], EntryPoint.Game.Renderer.SpeedFirstPlayer);
            this.AngleButtonHandler(PlayfieldModel.Players[0], EntryPoint.Game.Renderer.AngleFirstPlayer);
            this.SpeedButtonHandler(PlayfieldModel.Players[1], EntryPoint.Game.Renderer.SpeedSecondPlayer);
            this.AngleButtonHandler(PlayfieldModel.Players[1], EntryPoint.Game.Renderer.AngleSecondPlayer);

            PlayfieldModel.Players[(PlayfieldModel.CurrentPlayerIndex + 1) % 2].CanShoot = false;
            PlayfieldModel.Players[PlayfieldModel.CurrentPlayerIndex].CanShoot = true;

            bool canClick = oldKeyboard.IsKeyUp(Keys.Space);

            if (keyboard.IsKeyDown(Keys.Space) && PlayfieldModel.Players[PlayfieldModel.CurrentPlayerIndex].CanShoot && canClick)
            {
                EntryPoint.Game.Renderer.LoadSoundEffect(EntryPoint.Game.Renderer.InGameShotSound);
                EntryPoint.Game.Renderer.PLayersUI[PlayfieldModel.CurrentPlayerIndex].Ball = UIInitializer.CreateCannonBallUI(EntryPoint.Game.Content);
                EntryPoint.Game.Renderer.PLayersUI[(PlayfieldModel.CurrentPlayerIndex + 1) % 2].Ball = UIInitializer.CreateCannonBallUI(EntryPoint.Game.Content);

                StateMachine.ChangeState();
            }
        }

        private static void SetPlayerData()
        {
            EntryPoint.Game.Renderer.PLayersUI[PlayfieldModel.CurrentPlayerIndex].SpeedValue = PlayfieldModel.Players[PlayfieldModel.CurrentPlayerIndex].Speed;
            EntryPoint.Game.Renderer.PLayersUI[PlayfieldModel.CurrentPlayerIndex].AngleValue = PlayfieldModel.Players[PlayfieldModel.CurrentPlayerIndex].Angle;
            EntryPoint.Game.Renderer.PLayersUI[(PlayfieldModel.CurrentPlayerIndex + 1) % 2].SpeedValue = PlayfieldModel.Players[(PlayfieldModel.CurrentPlayerIndex + 1) % 2].Speed;
            EntryPoint.Game.Renderer.PLayersUI[(PlayfieldModel.CurrentPlayerIndex + 1) % 2].AngleValue = PlayfieldModel.Players[(PlayfieldModel.CurrentPlayerIndex + 1) % 2].Angle;
            EntryPoint.Game.Renderer.PLayersUI[PlayfieldModel.CurrentPlayerIndex].Body.Sprite.Angle = EntryPoint.Game.Renderer.PLayersUI[PlayfieldModel.CurrentPlayerIndex].AngleValue;
        }

        private void SpeedButtonHandler(Player player, GameButton button)
        {
            MouseState mouse = EntryPoint.Game.MouseState;
            MouseState oldMouseState = EntryPoint.Game.OldMouseState;

            bool mouseOverSpeedDown = new Rectangle(30 + (SECOND_PLAYER_BUTTONS_X * player.Index), 10, 60, 50).Contains(mouse.X, mouse.Y);
            bool mouseOverSpeed1Up = new Rectangle(90 + (SECOND_PLAYER_BUTTONS_X * player.Index), 10, 60, 50).Contains(mouse.X, mouse.Y);

            bool canClick = oldMouseState.LeftButton == ButtonState.Released;

            if (mouse.LeftButton == ButtonState.Pressed && mouseOverSpeed1Up && player.CanShoot)
            {
                button.ChangeToClickedUpImage();

                if (canClick)
                {
                    player.Speed += 1;
                }
                    
            }
            else if (mouse.LeftButton == ButtonState.Pressed && mouseOverSpeedDown && player.CanShoot)
            {
                button.ChangeToClickedDownImage();

                if (canClick)
                {
                    player.Speed -= 1;
                }
            }
            else if (player.CanShoot)
            {
                button.ChangetoActiveImage();
            }
            else
            {
                button.ChangeToInactiveImage();
            }
        }

        private void AngleButtonHandler(Player player, GameButton button)
        {
            MouseState mouse = EntryPoint.Game.MouseState;
            MouseState oldMouseState = EntryPoint.Game.OldMouseState;

            bool mouseOverAngleUp = new Rectangle(260 + (650 * player.Index), 10, 60, 50).Contains(mouse.X, mouse.Y);
            bool mouseOverAngleDown = new Rectangle(200 + (650 * player.Index), 10, 60, 50).Contains(mouse.X, mouse.Y);

            bool canClick = oldMouseState.LeftButton == ButtonState.Released;

            if (mouse.LeftButton == ButtonState.Pressed && mouseOverAngleUp && player.CanShoot)
            {
                button.ChangeToClickedUpImage();
                
                if (canClick)
                {
                    player.Angle += ONE_DEGREE_IN_RADIANS;
                }
            }
            else if (mouse.LeftButton == ButtonState.Pressed && mouseOverAngleDown && player.CanShoot)
            {
                button.ChangeToClickedDownImage();
                
                if (canClick)
                {
                    player.Angle -= ONE_DEGREE_IN_RADIANS;
                }
            }
            else if (player.CanShoot)
            {
                button.ChangetoActiveImage();
            }
            else
            {
                button.ChangeToInactiveImage();
            }
        }
    }
}