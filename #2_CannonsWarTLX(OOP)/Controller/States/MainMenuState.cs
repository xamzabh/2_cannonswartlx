﻿namespace CannonsWarTLX.Controller.States
{
    using CannonsWarTLX.Model;
    using CannonsWarTLX.View.Renderers;
    using CannonsWarTLX.View.UI;
    using Microsoft.Xna.Framework.Input;

    public class MainMenuState : State
    {
        public MainMenuState(State nextState)
            : base(nextState)
        {
        }

        public override void Execute()
        {
            EntryPoint.Game.Renderer.DrawMainMenu();
            PlayfieldModel.InitializePlayfieldModel();

            this.ActivatePlayButton();
            this.ActivateInfoButton();
            this.ActivateQuitButton();
        }

        public override void Draw(MonoGameRenderer renderer)
        {
            renderer.DrawMainMenu();
        }

        private void ActivatePlayButton()
        {
            MenuButton playButton = EntryPoint.Game.Renderer.PlayButton;
            MouseState mouse = EntryPoint.Game.MouseState;
            bool mouseOverPlayButton = playButton.Sprite.Rectangle.Contains(Mouse.GetState().X, Mouse.GetState().Y);

            bool canClick = EntryPoint.Game.OldMouseState.LeftButton == ButtonState.Released;

            if (mouse.LeftButton == ButtonState.Pressed && mouseOverPlayButton)
            {
                if (canClick)
                {                    
                    EntryPoint.Game.Renderer.LoadSoundEffect(EntryPoint.Game.Renderer.MenuOnClickSound);
                    this.NextState = StateMachine.States["PlayerTurnState"];
                    EntryPoint.Game.Renderer.LoadBackgroundSound(EntryPoint.Game.Renderer.InGameBackgroundSound);
                    StateMachine.ChangeState();
                }
            }
            else if (mouseOverPlayButton)
            {
                playButton.ChangeToOnHover();
            }
            else
            {
                playButton.ChangeToNormal();
            }
        }

        private void ActivateInfoButton()
        {
            MenuButton infoButton = EntryPoint.Game.Renderer.InfoButton;
            MouseState mouse = EntryPoint.Game.MouseState;
            bool mouseOverInfoButton = infoButton.Sprite.Rectangle.Contains(mouse.X, mouse.Y);

            bool canClick = EntryPoint.Game.OldMouseState.LeftButton == ButtonState.Released;

            if (mouse.LeftButton == ButtonState.Pressed && mouseOverInfoButton)
            {
                if (canClick)
                {
                    EntryPoint.Game.Renderer.LoadSoundEffect(EntryPoint.Game.Renderer.MenuOnClickSound);
                    this.NextState = StateMachine.States["InfoMenuState"];
                    StateMachine.ChangeState();
                }
            }
            else if (mouseOverInfoButton)
            {
                infoButton.ChangeToOnHover();
            }
            else
            {
                infoButton.ChangeToNormal();
            }
        }

        private void ActivateQuitButton()
        {
            MenuButton quitButton = EntryPoint.Game.Renderer.QuitButton;
            MouseState mouse = EntryPoint.Game.MouseState;
            bool mouseOverQuitButton = quitButton.Sprite.Rectangle.Contains(mouse.X, mouse.Y);

            bool canClick = EntryPoint.Game.OldMouseState.LeftButton == ButtonState.Released;

            if (mouse.LeftButton == ButtonState.Pressed && mouseOverQuitButton)
            {
                if (canClick)
                {
                    EntryPoint.Game.Renderer.LoadSoundEffect(EntryPoint.Game.Renderer.MenuOnClickSound);
                    EntryPoint.Game.Exit();
                }
            }
            else if (mouseOverQuitButton)
            {
                quitButton.ChangeToOnHover();
            }
            else
            {
                quitButton.ChangeToNormal();
            }
        }
    }
}
