﻿namespace CannonsWarTLX.Controller.States
{
    using CannonsWarTLX.View.Renderers;
    using CannonsWarTLX.View.UI;
    using Microsoft.Xna.Framework.Input;

    public class InfoMenuState : State
    {
        public InfoMenuState(State nextState)
            : base(nextState)
        {
        }

        public override void Execute()
        {
            EntryPoint.Game.Renderer.DrawInfoMenu();
            this.ActivateBackButton();
        }

        public override void Draw(MonoGameRenderer renderer)
        {
            renderer.DrawInfoMenu();
        }

        private void ActivateBackButton()
        {
            MenuButton backButton = EntryPoint.Game.Renderer.BackButton;
            bool mouseOverBackButton = backButton.Sprite.Rectangle.Contains(Mouse.GetState().X, Mouse.GetState().Y);

            bool canClick = EntryPoint.Game.OldMouseState.LeftButton == ButtonState.Released;
            if (Mouse.GetState().LeftButton == ButtonState.Pressed && mouseOverBackButton)
            {
                if (canClick)
                {
                    EntryPoint.Game.Renderer.LoadSoundEffect(EntryPoint.Game.Renderer.MenuOnClickSound);
                    StateMachine.ChangeState();
                }
            }
            else if (mouseOverBackButton)
            {
                backButton.ChangeToOnHover();
            }
            else
            {
                backButton.ChangeToNormal();
            }
        }
    }
}
