﻿namespace CannonsWarTLX.Controller.States
{
    using System;
    using System.Collections.Generic;
    using CannonsWarTLX.Model;
    using CannonsWarTLX.Model.Players;
    using CannonsWarTLX.View;
    using CannonsWarTLX.View.UI;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using Microsoft.Xna.Framework.Input;

    public class ShootState : State
    {
        public const float EARTH_ACCELERATION_G = -9.8f;

        private int newX;
        private int newY;

        private float time;

        public ShootState(State nextState)
            : base(nextState)
        {
        }

        public Color[] CollisionMapData { get; set; }

        public Texture2D CollisionMap { get; set; }

        public override void Execute()
        {
            this.CollisionMap = EntryPoint.Game.Renderer.CurrentMap.Image;
            this.CollisionMapData = new Color[EntryPoint.Game.Renderer.CurrentMap.Image.Width * EntryPoint.Game.Renderer.CurrentMap.Image.Height];
            EntryPoint.Game.Renderer.CurrentMap.Image.GetData<Color>(this.CollisionMapData);

            this.InitializeShooting(PlayfieldModel.Players[PlayfieldModel.CurrentPlayerIndex], EntryPoint.Game.Renderer.PLayersUI[PlayfieldModel.CurrentPlayerIndex]);
        }

        public override void Draw(View.Renderers.MonoGameRenderer renderer)
        {
            renderer.DrawGameEnvironment();
            renderer.DrawCannonBall(EntryPoint.Game.Renderer.PLayersUI[PlayfieldModel.CurrentPlayerIndex], PlayfieldModel.Players[PlayfieldModel.CurrentPlayerIndex].CanShoot);
        }

        private void InitializeShooting(Player player, CannonUI cannon)
        {
            int playerMultiplier = 0;

            if (player.Index == 0)
            {
                playerMultiplier = 1;
            }
            else
            {
                playerMultiplier = -1;
            }

            if (!this.Collision(player, cannon) && PlayfieldModel.Players[PlayfieldModel.CurrentPlayerIndex].CanShoot)
            {
                // TODO: Balls Should Be SHot exavtly from the Cannon Body
                this.newX = (int)((cannon.Body.Sprite.Rectangle.X  + playerMultiplier * (this.time * player.Speed * (float)Math.Cos(-player.Angle * playerMultiplier))));
                this.newY = (int)(cannon.Body.Sprite.Rectangle.Y  - (this.time * player.Speed * (float)Math.Sin(-player.Angle * playerMultiplier) + 0.5f * EARTH_ACCELERATION_G * this.time * this.time)); // 0.5f comes from the formula itself
                this.time += 0.3f;

                cannon.Ball.Sprite.Rectangle = new Rectangle(this.newX, this.newY, cannon.Ball.Sprite.Image.Width, cannon.Ball.Sprite.Image.Height);
            }

            if (!player.CanShoot)
            {
                this.newX = 0;
                this.newY = 0;
                this.time = 0f;

                PlayfieldModel.CurrentPlayerIndex = (PlayfieldModel.CurrentPlayerIndex + 1) % 2;
                PlayfieldModel.Players[(PlayfieldModel.CurrentPlayerIndex + 1) % 2].CanShoot = true;

                StateMachine.CurrentState.NextState = StateMachine.States["PlayerTurnState"];
                StateMachine.ChangeState();
            }
        }

        private bool Collision(Player player, CannonUI cannon)
        {
            int[] index =
            {
                 ((int)cannon.Ball.Sprite.Rectangle.Y + 10) * EntryPoint.Game.Renderer.CurrentMap.Image.Width + (int)cannon.Ball.Sprite.Rectangle.X + cannon.Ball.Sprite.Rectangle.Width - 10   
            };

            for (int i = 0; i < index.Length; i++)
            {
                if (!this.CheckOutOfMapBounds(player, cannon, index, i))
                {
                    if (this.CheckMapCollision(player, cannon, index, i))
                    {
                        EntryPoint.Game.Renderer.LoadSoundEffect(EntryPoint.Game.Renderer.InGameCollisionExplosionSound);
                        return true;
                    }

                    if (this.CheckPlayerCollision(player, cannon))
                    {
                        EntryPoint.Game.Renderer.LoadSoundEffect(EntryPoint.Game.Renderer.InGameCollisionExplosionSound);
                        StateMachine.CurrentState.NextState = StateMachine.States["GameOverState"];
                        StateMachine.ChangeState();
                        return true;
                    }
                }
            }

            return false;
        }

        private bool CheckOutOfMapBounds(Player player, CannonUI cannon, int[] index, int i)
        {
            if (cannon.Ball.Sprite.Rectangle.X < 0 ||
                cannon.Ball.Sprite.Rectangle.Y < 0 ||
                index[i] > this.CollisionMap.Height * this.CollisionMap.Width - 1)
            {
                player.CanShoot = false;

                return true; // Out of the bounds of the game window
            }

            return false;
        }

        private bool CheckMapCollision(Player player, CannonUI cannon, int[] index, int i)
        {
            if (this.CollisionMapData[index[i]] != Color.Transparent)
            {
                this.DrawDiscreteCircle(cannon);

                player.CanShoot = false;

                return true;
            }

            return false;
        }

        private bool CheckPlayerCollision(Player player, CannonUI cannon)
        {
            if (EntryPoint.Game.Renderer.PLayersUI[(PlayfieldModel.CurrentPlayerIndex + 1) % 2].Body.Sprite.Rectangle.Intersects(cannon.Ball.Sprite.Rectangle))
            {
                player.IsAlive = false;
                return true;
            }

            return false;
        }

        private void DrawDiscreteCircle(CannonUI cannon)
        {
            for (float n = 0; n < MathHelper.TwoPi; n += 0.01f)
            {
                for (float j = 0; j < cannon.Ball.Sprite.Image.Width / 1.3f; j += 1)
                {
                    int x = (int)(cannon.Ball.Sprite.Rectangle.X + Math.Ceiling(j * Math.Sin(n)));
                    int y = (int)(cannon.Ball.Sprite.Rectangle.Y + Math.Ceiling(j * Math.Cos(n)));
                    int position = y * EntryPoint.Game.Renderer.CurrentMap.Image.Width + x;

                    if (!(x < 0 || y < 0 || x > EntryPoint.Game.Renderer.CurrentMap.Image.Width - 1 || y > EntryPoint.Game.Renderer.CurrentMap.Image.Height - 1))
                    {
                        this.CollisionMapData[position] = Color.Transparent;
                    }
                }
            }

            EntryPoint.Game.Renderer.CurrentMap.Image.SetData<Color>(this.CollisionMapData);
        }
    }
}