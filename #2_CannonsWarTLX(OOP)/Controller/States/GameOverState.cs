﻿namespace CannonsWarTLX.Controller.States
{
    using System.Threading;
    using CannonsWarTLX.Model;
    using CannonsWarTLX.View.Renderers;

    public class GameOverState : State
    {
        public GameOverState(State nextState)
            : base(nextState)
        {
        }

        public override void Execute()
        {
            this.NextState = StateMachine.States["MainMenuState"];
            Thread.Sleep(3000);
            StateMachine.ChangeState();
        }

        public override void Draw(MonoGameRenderer renderer)
        {
            base.Draw(renderer);
            renderer.ShowWinner(renderer.PLayersUI[PlayfieldModel.CurrentPlayerIndex]);
        }
    }
}
