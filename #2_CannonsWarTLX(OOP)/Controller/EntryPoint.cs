﻿namespace CannonsWarTLX.Controller
{
    public static class EntryPoint
    {
        public static CannonsWarGame Game { get; set; }

        public static void Main()
        {
            using (Game = new CannonsWarGame())
            {
                Game.Run();
            }
        }
    }
}
