﻿namespace CannonsWarTLX
{
    using CannonsWarTLX.Controller;
    using CannonsWarTLX.View;
    using CannonsWarTLX.View.Renderers;
    using CannonsWarTLX.View.UI;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using Microsoft.Xna.Framework.Input;

    public class CannonsWarGame : Game
    {
        public const int GAME_WIDTH = 1000;
        public const int GAME_HEIGHT = 600;

        public CannonsWarGame()
        {
            Content.RootDirectory = "Content";
            this.Graphics = new GraphicsDeviceManager(this);
            this.Graphics.PreferredBackBufferHeight = GAME_HEIGHT;
            this.Graphics.PreferredBackBufferWidth = GAME_WIDTH;
            this.IsMouseVisible = true;
        }

        public GraphicsDeviceManager Graphics { get; set; }

        public MouseState MouseState { get; set; }

        public MouseState OldMouseState { get; set; }

        public KeyboardState KeyboardState { get; set; }

        public KeyboardState OldKeyboardState { get; set; }

        public MonoGameRenderer Renderer { get; set; }

        public SpriteBatch SpriteBatch { get; private set; }

        protected override void Initialize()
        {
            this.SpriteBatch = new SpriteBatch(GraphicsDevice);
            this.Renderer = new MonoGameRenderer();
            StateMachine.Initialize();
            StateMachine.CurrentState = StateMachine.States["MainMenuState"];
            base.Initialize();
        }

        protected override void LoadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {
            this.MouseState = Mouse.GetState();
            this.KeyboardState = Keyboard.GetState();

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            this.GraphicsDevice.Clear(Color.CornflowerBlue);

            this.SpriteBatch.Begin();
            StateMachine.CurrentState.Execute();
            StateMachine.CurrentState.Draw(this.Renderer);
            this.OldMouseState = this.MouseState;
            this.OldKeyboardState = this.KeyboardState;
            this.SpriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
