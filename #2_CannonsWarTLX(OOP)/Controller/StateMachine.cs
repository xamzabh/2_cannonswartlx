﻿namespace CannonsWarTLX.Controller
{
    using System.Collections.Generic;
    using CannonsWarTLX.Controller.States;

    public static class StateMachine
    {
        public static Dictionary<string, State> States = new Dictionary<string, State>();
        private static MainMenuState mainMenu;
        private static InfoMenuState infoMenu;
        private static PlayerTurnState playerTurn;
        private static ShootState shoot;
        private static GameOverState gameOver;

        public static State CurrentState { get; set; }

        public static void Initialize()
        {
            mainMenu = new MainMenuState(mainMenu);
            infoMenu = new InfoMenuState(mainMenu);
            shoot = new ShootState(playerTurn);
            playerTurn = new PlayerTurnState(shoot);
            gameOver = new GameOverState(mainMenu);
            States.Add("MainMenuState", mainMenu);
            States.Add("InfoMenuState", infoMenu);
            States.Add("PlayerTurnState", playerTurn);
            States.Add("ShootState", shoot);
            States.Add("GameOverState", gameOver);
        }

        public static void ChangeState()
        {
            CurrentState = CurrentState.NextState;
        }
    }
}
