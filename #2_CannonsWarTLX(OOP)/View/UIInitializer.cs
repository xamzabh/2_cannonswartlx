﻿namespace CannonsWarTLX.View
{
    using System;
    using CannonsWarTLX.View.UI;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Audio;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;
    using Microsoft.Xna.Framework.Media;

    public static class UIInitializer
    {
        public const float BUFF_ON_HOVER = 1.06F;
        public const int MOVE_COMPATED_TO_THE_CENTER = 120;
        public const int GAME_WIDTH = 1000;
        public const int GAME_HEIGHT = 600;

        private static Random rng = new Random();

        // Create Menu
        public static Background CreateMenuBackground(ContentManager content)
        {
            content.RootDirectory = @"Content\Backgrounds\Menu";
            Texture2D backgroundImage = content.Load<Texture2D>("Background");
            Rectangle backgroundRectangle = new Rectangle(GAME_WIDTH / 2 - backgroundImage.Width / 2, GAME_HEIGHT / 2 - backgroundImage.Height / 2, backgroundImage.Width, backgroundImage.Height);
            Sprite backgroundSprite = new Sprite(backgroundRectangle, backgroundImage);

            Background background = new Background(backgroundSprite);
            return background;
        }

        public static MenuButton CreatePlayButton(ContentManager content)
        {
            content.RootDirectory = @"Content\Buttons\Menu";

            Texture2D playButtonImage = content.Load<Texture2D>("Button_play");
            Rectangle playButtonRectangleUp = new Rectangle((int)(GAME_WIDTH / 2 - playButtonImage.Width / 2), GAME_HEIGHT / 2 - playButtonImage.Height / 2 - MOVE_COMPATED_TO_THE_CENTER, playButtonImage.Width, playButtonImage.Height);

            Sprite playButtonSprite = new Sprite(playButtonRectangleUp, playButtonImage);
            var playButton = new MenuButton(playButtonSprite, playButtonImage, playButtonImage);
            playButton.BuffedRectangle = new Rectangle(GAME_WIDTH / 2 - (int)(playButtonImage.Width * BUFF_ON_HOVER) / 2, GAME_HEIGHT / 2 - playButtonImage.Height / 2 - MOVE_COMPATED_TO_THE_CENTER, (int)(playButtonImage.Width * BUFF_ON_HOVER), (int)(playButtonImage.Height * BUFF_ON_HOVER));
            playButton.NormalRectangle = playButtonRectangleUp;

            return playButton;
        }

        public static MenuButton CreateInfoButton(ContentManager content)
        {
            content.RootDirectory = @"Content\Buttons\Menu";
            Texture2D infoButtonImage = content.Load<Texture2D>("Button_info");
            Rectangle infoButtonRectangleUp = new Rectangle(GAME_WIDTH / 2 - infoButtonImage.Width / 2, GAME_HEIGHT / 2 - infoButtonImage.Height / 2, infoButtonImage.Width, infoButtonImage.Height);

            Sprite infoButtonSprite = new Sprite(infoButtonRectangleUp, infoButtonImage);
            var infoButton = new MenuButton(infoButtonSprite, infoButtonImage, infoButtonImage);
            infoButton.BuffedRectangle = new Rectangle(GAME_WIDTH / 2 - (int)(infoButtonImage.Width * BUFF_ON_HOVER) / 2, GAME_HEIGHT / 2 - infoButtonImage.Height / 2, (int)(infoButtonImage.Width * BUFF_ON_HOVER), (int)(infoButtonImage.Height * BUFF_ON_HOVER));
            infoButton.NormalRectangle = infoButtonRectangleUp;

            return infoButton;
        }

        public static MenuButton CreateQuitButton(ContentManager content)
        {
            content.RootDirectory = @"Content\Buttons\Menu";
            Texture2D quitButtonImage = content.Load<Texture2D>("Button_quit");
            Rectangle quitButtonRectangleUp = new Rectangle(GAME_WIDTH / 2 - quitButtonImage.Width / 2, GAME_HEIGHT / 2 - quitButtonImage.Height / 2 + MOVE_COMPATED_TO_THE_CENTER, quitButtonImage.Width, quitButtonImage.Height);

            Sprite quitButtonSprite = new Sprite(quitButtonRectangleUp, quitButtonImage);
            var quitButton = new MenuButton(quitButtonSprite, quitButtonImage, quitButtonImage);
            quitButton.BuffedRectangle = new Rectangle(GAME_WIDTH / 2 - (int)(quitButtonImage.Width * BUFF_ON_HOVER) / 2, GAME_HEIGHT / 2 - quitButtonImage.Height / 2 + MOVE_COMPATED_TO_THE_CENTER, (int)(quitButtonImage.Width * BUFF_ON_HOVER), (int)(quitButtonImage.Height * BUFF_ON_HOVER));
            quitButton.NormalRectangle = quitButtonRectangleUp;

            return quitButton;
        }

        public static Background CreateInfoBackground(ContentManager content)
        {
            content.RootDirectory = @"Content\Backgrounds\Menu";
            Texture2D backgroundImage = content.Load<Texture2D>("InfoBackground");
            Rectangle backgroundRectangle = new Rectangle(GAME_WIDTH / 2 - backgroundImage.Width / 2, GAME_HEIGHT / 2 - backgroundImage.Height / 2, backgroundImage.Width, backgroundImage.Height);
            Sprite backgroundSprite = new Sprite(backgroundRectangle, backgroundImage);

            Background background = new Background(backgroundSprite);
            return background;
        }

        public static MenuButton CreateBackButton(ContentManager content)
        {
            content.RootDirectory = @"Content\Buttons\Menu";
            Texture2D backButtonImage = content.Load<Texture2D>("button_back");
            Rectangle backButtonRectangle = new Rectangle((int)(GAME_WIDTH / 1.5 - backButtonImage.Width / 1.6), GAME_HEIGHT / 2 - backButtonImage.Height / 2 + 172, backButtonImage.Width, backButtonImage.Height);

            Sprite backButtonSprite = new Sprite(backButtonRectangle, backButtonImage);
            var backButton = new MenuButton(backButtonSprite, backButtonImage, backButtonImage);
            backButton.BuffedRectangle = new Rectangle((int)(GAME_WIDTH / 1.5 - ((int)(backButtonImage.Width * BUFF_ON_HOVER) / 1.6)), GAME_HEIGHT / 2 - backButtonImage.Height / 2 + 172, (int)(backButtonImage.Width * BUFF_ON_HOVER), (int)(backButtonImage.Height * BUFF_ON_HOVER));
            backButton.NormalRectangle = backButtonRectangle;

            return backButton;
        }

        // Create InGame UI
        public static Background CreateBackground(ContentManager content)
        {
            content.RootDirectory = @"Content\Backgrounds\InGame";
            Texture2D backgroundImage = content.Load<Texture2D>("Background1");
            Rectangle backgroundRectangle = new Rectangle(0, 0, 1000, 600);
            Sprite backgroundSprite = new Sprite(backgroundRectangle, backgroundImage);

            Background background = new Background(backgroundSprite);
            return background;
        }

        public static SpriteFont CreateFont(ContentManager content)
        {
            content.RootDirectory = "Content";
            SpriteFont font = content.Load<SpriteFont>("Font");
            return font;
        }

        public static Map CreateMap(ContentManager content)
        {
            content.RootDirectory = @"Content\Maps";
            Texture2D map = content.Load<Texture2D>("Map" + rng.Next(1, 3).ToString());
            Map currentMap = new Map(new Vector2(0, 0), map);

            return currentMap;
        }

        public static GameButton CreateSpeedButton(ContentManager content, int index)
        {
            content.RootDirectory = @"Content\Buttons\InGame";
            Texture2D speedActiveImage = content.Load<Texture2D>("Speed");
            Texture2D speedClickedUpImage = content.Load<Texture2D>("SpeedClickedUp");
            Texture2D speedClickedDownImage = content.Load<Texture2D>("SpeedClickedDown");
            Texture2D speedInactiveImage = content.Load<Texture2D>("SpeedInactive");
            Rectangle speedRectangleImage = new Rectangle((int)(GAME_WIDTH * 0.03 + (640 * index)), 10, speedActiveImage.Width, speedActiveImage.Height);

            Sprite speedSprite = new Sprite(speedRectangleImage, speedActiveImage);
            GameButton speedButton = new GameButton(speedSprite, speedActiveImage, speedClickedUpImage, speedClickedDownImage, speedInactiveImage);

            return speedButton;
        }

        public static GameButton CreateAngleButton(ContentManager content, int index)
        {
            content.RootDirectory = @"Content\Buttons\InGame";
            Texture2D angleActiveImage = content.Load<Texture2D>("Angle");
            Texture2D angleClickedImage = content.Load<Texture2D>("AngleClickedUp");
            Texture2D angleCLickedDownImage = content.Load<Texture2D>("AngleClickedDown");
            Texture2D angleInactiveImage = content.Load<Texture2D>("AngleInactive");
            Rectangle angleRectangleUp = new Rectangle((int)(GAME_WIDTH * 0.2 + (640 * index)), 10, angleActiveImage.Width, angleActiveImage.Height);

            Sprite angleSprite = new Sprite(angleRectangleUp, angleActiveImage);

            GameButton angleButton = new GameButton(angleSprite, angleActiveImage, angleClickedImage, angleCLickedDownImage, angleInactiveImage);

            return angleButton;
        }

        public static CannonUI CreateCannonUI(ContentManager content, int index, int x, int y)
        {
            content.RootDirectory = @"Content\CannonParts";
            
            // Create Base
            Texture2D baseImage = content.Load<Texture2D>("CannonBase");
            Rectangle baseRectangle = new Rectangle(x, y, (int)(baseImage.Width / 1.3), (int)(baseImage.Height / 1.3));

            Sprite baseSprite = new Sprite(baseRectangle, baseImage);
            CannonUIBase cannonBase = new CannonUIBase(baseSprite);

            // Create Body
            Texture2D bodyImage = content.Load<Texture2D>("CannonBody" + index.ToString());
            Rectangle bodyRectangle = new Rectangle(x + 15, y, bodyImage.Width / 2, bodyImage.Height / 2);

            Sprite bodySprite = new Sprite(bodyRectangle, bodyImage);
            CannonUIBody body = new CannonUIBody(bodySprite, index);

            // Create Ball
            CannonUIBall ball = CreateCannonBallUI(content);

            // Create Cannon(Body,Base)
            CannonUI cannon = new CannonUI(body, cannonBase, index);
            cannon.Ball = ball;

            return cannon;
        }

        public static CannonUIBall CreateCannonBallUI(ContentManager content)
        {
            content.RootDirectory = @"Content\CannonParts";
            Texture2D ballImage = content.Load<Texture2D>("CannonBall");
            Rectangle ballRectangle = new Rectangle(1000, 300, ballImage.Width, ballImage.Height);

            Sprite ballSprite = new Sprite(ballRectangle, ballImage);
            CannonUIBall ball = new CannonUIBall(ballSprite);

            return ball;
        }

        // Create Sounds
        public static Song InitializeBackgroundSound(ContentManager content)
        {
            content.RootDirectory = @"Content\Sounds\InGame";
            Song backgroundSound = content.Load<Song>("BackgroundWindSound");

            return backgroundSound;
        }

        public static SoundEffect InitializeSoundEffect(ContentManager content, string path)
        {
            content.RootDirectory = @"Content\Sounds";
            SoundEffect soundEffect = content.Load<SoundEffect>(path);
            return soundEffect;
        }
    }
}
