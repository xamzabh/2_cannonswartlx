﻿namespace CannonsWarTLX.View.Renderers
{
    public abstract class AbstractRenderer
    {
        public abstract void DrawMainMenu();

        public abstract void DrawInfoMenu();

        public abstract void DrawGameEnvironment();
    }
}
