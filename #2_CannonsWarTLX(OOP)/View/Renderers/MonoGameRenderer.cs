﻿namespace CannonsWarTLX.View.Renderers
{
    using System;
    using System.Collections.Generic;
    using CannonsWarTLX.Controller;
    using CannonsWarTLX.Controller.States;
    using CannonsWarTLX.Model;
    using CannonsWarTLX.View.UI;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Audio;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;
    using Microsoft.Xna.Framework.Media;

    public class MonoGameRenderer : AbstractRenderer
    {
        private ContentManager content = EntryPoint.Game.Content;
        private SpriteBatch spriteBatch;

        public MonoGameRenderer()
        {
            this.spriteBatch = EntryPoint.Game.SpriteBatch;
            this.Font = UIInitializer.CreateFont(this.content);

            this.MenuBackground = UIInitializer.CreateMenuBackground(this.content);
            this.PlayButton = UIInitializer.CreatePlayButton(this.content);
            this.InfoButton = UIInitializer.CreateInfoButton(this.content);
            this.InfoBackground = UIInitializer.CreateInfoBackground(this.content);
            this.QuitButton = UIInitializer.CreateQuitButton(this.content);
            this.BackButton = UIInitializer.CreateBackButton(this.content);

            this.InGameBackground = UIInitializer.CreateBackground(this.content);
            this.CurrentMap = UIInitializer.CreateMap(this.content);

            this.PLayersUI = new List<CannonUI>();

            this.CollisionMapData = new Color[this.CurrentMap.Image.Width * this.CurrentMap.Image.Height];
            this.CurrentMap.Image.GetData<Color>(this.CollisionMapData);

            this.Rng = new Random();
            this.FIrstPlayerCoord = this.GetBaseCoordinates(0);
            this.SecondPlayerCoord = this.GetBaseCoordinates(1);

            this.FirstPlayer = UIInitializer.CreateCannonUI(this.content, 0, this.FIrstPlayerCoord[0], this.FIrstPlayerCoord[1]);
            this.SecondPlayer = UIInitializer.CreateCannonUI(this.content, 1, this.SecondPlayerCoord[0], this.SecondPlayerCoord[1]);

            this.PLayersUI.Add(this.FirstPlayer);
            this.PLayersUI.Add(this.SecondPlayer);

            this.SpeedFirstPlayer = UIInitializer.CreateSpeedButton(this.content, this.PLayersUI.IndexOf(this.FirstPlayer));
            this.AngleFirstPlayer = UIInitializer.CreateAngleButton(this.content, this.PLayersUI.IndexOf(this.FirstPlayer));
            this.SpeedSecondPlayer = UIInitializer.CreateSpeedButton(this.content, this.PLayersUI.IndexOf(this.SecondPlayer));
            this.AngleSecondPlayer = UIInitializer.CreateAngleButton(this.content, this.PLayersUI.IndexOf(this.SecondPlayer));

            this.InGameBackgroundSound = UIInitializer.InitializeBackgroundSound(this.content);
            this.MenuOnClickSound = UIInitializer.InitializeSoundEffect(this.content, @"Menu\OnHoverTick");
            this.InGameShotSound = UIInitializer.InitializeSoundEffect(this.content, @"InGame\ShotSound");
            this.InGameCollisionExplosionSound = UIInitializer.InitializeSoundEffect(this.content, @"InGame\ExplosionSound");
        }

        public SpriteFont Font { get; set; }

        public SoundEffect MenuOnClickSound { get; set; }

        public Background MenuBackground { get; set; }

        public MenuButton PlayButton { get; set; }
        
        public MenuButton InfoButton { get; set; }
        
        public Background InfoBackground { get; set; }
        
        public MenuButton QuitButton { get; set; }
        
        public MenuButton BackButton { get; set; }

        public SoundEffect InGameShotSound { get; set; }
        
        public SoundEffect InGameCollisionExplosionSound { get; set; }

        public Song InGameBackgroundSound { get; set; }

        public Background InGameBackground { get; set; }
        
        public Map CurrentMap { get; set; }
        
        public Color[] CollisionMapData { get; set; }

        public Random Rng { get; set; }

        public List<CannonUI> PLayersUI { get; set; }
        
        public int[] FIrstPlayerCoord { get; set; }
        
        public int[] SecondPlayerCoord { get; set; }
        
        public CannonUI FirstPlayer { get; set; }
        
        public CannonUI SecondPlayer { get; set; }

        public GameButton SpeedFirstPlayer { get; set; }
        
        public GameButton AngleFirstPlayer { get; set; }
        
        public GameButton SpeedSecondPlayer { get; set; }
        
        public GameButton AngleSecondPlayer { get; set; }

        public override void DrawMainMenu()
        {
            this.spriteBatch = EntryPoint.Game.SpriteBatch;
            this.MenuBackground.Draw(this.spriteBatch);
            this.PlayButton.Draw(this.spriteBatch);
            this.InfoButton.Draw(this.spriteBatch);
            this.QuitButton.Draw(this.spriteBatch);
        }

        public override void DrawInfoMenu()
        {
            this.InfoBackground.Draw(this.spriteBatch);
            this.BackButton.Draw(this.spriteBatch);
        }

        public override void DrawGameEnvironment()
        {
            this.spriteBatch = EntryPoint.Game.SpriteBatch;
            this.Font = UIInitializer.CreateFont(this.content);
            this.InGameBackground.Draw(this.spriteBatch);
            this.CurrentMap.Draw(this.spriteBatch);

            foreach (var player in this.PLayersUI)
            {
                player.Draw(this.spriteBatch);
            }

            this.SpeedFirstPlayer.Draw(this.spriteBatch);
            this.AngleFirstPlayer.Draw(this.spriteBatch);
            this.SpeedSecondPlayer.Draw(this.spriteBatch);
            this.AngleSecondPlayer.Draw(this.spriteBatch);
            this.spriteBatch.DrawString(EntryPoint.Game.Renderer.Font, this.FirstPlayer.SpeedValue.ToString(), new Vector2(this.SpeedFirstPlayer.Sprite.Rectangle.X + this.SpeedFirstPlayer.Sprite.Image.Width / 2 - 10, 36), Color.Black);
            this.spriteBatch.DrawString(EntryPoint.Game.Renderer.Font, ((float)Math.Round(-this.FirstPlayer.AngleValue * 180 / Math.PI)).ToString(), new Vector2(this.AngleFirstPlayer.Sprite.Rectangle.X + this.AngleFirstPlayer.Sprite.Image.Width / 2 - 10, 36), Color.Black);
            this.spriteBatch.DrawString(EntryPoint.Game.Renderer.Font, this.SecondPlayer.SpeedValue.ToString(), new Vector2(640 + 80, 36), Color.Black);
            this.spriteBatch.DrawString(EntryPoint.Game.Renderer.Font, (-1 * (float)Math.Round(-this.SecondPlayer.AngleValue * 180 / Math.PI)).ToString(), new Vector2(650 + 230, 36), Color.Black);
        }

        public void DrawCannonBall(CannonUI cannon, bool canDraw)
        {
            cannon.Ball.Draw(this.spriteBatch, canDraw);
        }

        public void ShowWinner(CannonUI cannon)
        {
            Texture2D winner = this.content.Load<Texture2D>("Winner");
            this.spriteBatch.Draw(winner, new Rectangle(cannon.Base.Sprite.Rectangle.X + cannon.Base.Sprite.Image.Width / 2 - winner.Width / 2, cannon.Base.Sprite.Rectangle.Y - 300, winner.Width, winner.Height), Color.White);
        }

        public void LoadBackgroundSound(Song backgroundSong)
        {
            MediaPlayer.IsRepeating = true;
            MediaPlayer.Play(backgroundSong);
        }

        public void LoadSoundEffect(SoundEffect soundEffect)
        {
            soundEffect.Play();
        }

        private int[] GetBaseCoordinates(int index)
        {
            int x = this.Rng.Next(20, 250) + this.Rng.Next(550, 650) * index;
            int y = 10;

            while (this.CollisionMapData[(y + 15) * 1000 + x + 46 / 2] == Color.Transparent)
            {
                y++;
            }

            return new int[] { x, y };
        }
    }
}
