﻿namespace CannonsWarTLX.View.UI
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;

    public abstract class Button
    {
        public Button(Sprite sprite)
        {
            this.Sprite = sprite;
        }

        public Sprite Sprite { get; private set; }

        public void Draw(SpriteBatch spritebatch)
        {
            spritebatch.Draw(this.Sprite.Image, this.Sprite.Rectangle, Color.White);
        }
    }
}
