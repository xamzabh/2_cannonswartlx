﻿namespace CannonsWarTLX.View.UI
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;

    public class MenuButton : Button
    {
        private Texture2D inactiveImage;
        private Texture2D onHoverImage;

        public MenuButton(Sprite sprite, Texture2D inactiveImage, Texture2D onHoverImage)
            : base(sprite)
        {
            this.inactiveImage = inactiveImage;
            this.onHoverImage = onHoverImage;
        }

        public Rectangle BuffedRectangle { get; set; }

        public Rectangle NormalRectangle { get; set; }

        public void ChangeToOnHover()
        {
            this.Sprite.Image = this.onHoverImage;
            this.Sprite.Rectangle = this.BuffedRectangle;
        }

        public void ChangeToNormal()
        {
            this.Sprite.Image = this.inactiveImage;
            this.Sprite.Rectangle = this.NormalRectangle;
        }
    }
}
