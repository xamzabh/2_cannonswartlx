﻿namespace CannonsWarTLX.View.UI
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;

    public class CannonUIBall
    {
        public CannonUIBall(Sprite sprite)
        {
            this.Sprite = sprite;
        }

        public Sprite Sprite { get; set; }

        public void Draw(SpriteBatch sb, bool canDraw)
        {
            if (canDraw)
            {
                sb.Draw(this.Sprite.Image, this.Sprite.Rectangle, Color.White);
            }
        }
    }
}
