﻿namespace CannonsWarTLX.View.UI
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;

    public class GameButton : Button
    {
        private Texture2D clickedUpImage;
        private Texture2D clickedDownImage;
        private Texture2D inactiveImage;
        private Texture2D activeImage;

        public GameButton(Sprite sprite, Texture2D activeImage, Texture2D clickedUpImage, Texture2D clickedDownImage, Texture2D inactiveImage)
            : base(sprite)
        {
            this.activeImage = activeImage;
            this.clickedUpImage = clickedUpImage;
            this.clickedDownImage = clickedDownImage;
            this.inactiveImage = inactiveImage;
        }
       
        public void ChangeToClickedUpImage()
        {
            this.Sprite.Image = this.clickedUpImage;
        }

        public void ChangeToClickedDownImage()
        {
            this.Sprite.Image = this.clickedDownImage;
        }

        public void ChangeToInactiveImage()
        {
            this.Sprite.Image = this.inactiveImage;
        }

        public void ChangetoActiveImage()
        {
            this.Sprite.Image = this.activeImage;
        }
    }
}
