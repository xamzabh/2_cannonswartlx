﻿namespace CannonsWarTLX.View.UI
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;

    public class Map
    {
        public Map(Vector2 location, Texture2D image)
        {
            this.Location = location;
            this.Image = image;
            this.Bounds = new Rectangle(0, 0, 1000, 600);
        }

        public Texture2D Image { get; set; }
        
        public Vector2 Location { get; set; }

        public Rectangle Bounds { get; set; }

        public void Draw(SpriteBatch sb)
        {
            sb.Draw(this.Image, this.Location);
        }
    }
}
