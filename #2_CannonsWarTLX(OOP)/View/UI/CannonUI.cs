﻿namespace CannonsWarTLX.View.UI
{
    using Microsoft.Xna.Framework.Graphics;

    public class CannonUI
    {
        public CannonUI(CannonUIBody cannonBody, CannonUIBase cannonBase, int index)
        {
            this.Body = cannonBody;
            this.Base = cannonBase;
            this.Index = index;
        }

        public CannonUIBody Body { get; set; }

        public CannonUIBase Base { get; set; }

        public CannonUIBall Ball { get; set; }

        public int Index { get; set; }

        public float SpeedValue { get; set; }

        public float AngleValue { get; set; }

        public void Draw(SpriteBatch spriteBatch)
        {
            this.Body.Draw(spriteBatch, this.Index);
            this.Base.Draw(spriteBatch);
        }
    }
}
