﻿namespace CannonsWarTLX.View.UI
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;

    public class CannonUIBody
    {
        public CannonUIBody(Sprite sprite, int index)
        {
            this.Sprite = sprite;
        }

        public Sprite Sprite { get; set; }

        public void Draw(SpriteBatch spriteBatch, int index)
        {
            spriteBatch.Draw(this.Sprite.Image, this.Sprite.Rectangle, null, Color.White, this.Sprite.Angle, new Vector2(30 + 90 * index, 30), SpriteEffects.None, 0f);
        }
    }
}
