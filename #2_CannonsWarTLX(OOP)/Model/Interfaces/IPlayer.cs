﻿namespace CannonsWarTLX.Model.Interfaces
{
    public interface IPlayer
    {
        float Speed { get; set; }

        float Angle { get; set; }

        bool CanShoot { get; set; }

        bool IsAlive { get; set; }
    }
}
