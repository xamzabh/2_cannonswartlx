﻿namespace CannonsWarTLX.Model.Players
{
    using System;
    using CannonsWarTLX.Model.Interfaces;

    public class Player : IPlayer
    {
        private int index;

        public Player(int index, float angle)
        {
            this.Index = index;
            this.Angle = angle;
            this.Speed = 50;
            this.CanShoot = true;
            this.IsAlive = true;
        }

        public int Index
        {
            get
            {
                return this.index;
            }

            set
            {
                if (this.index < 0 || this.index > 1)
                {
                    throw new IndexOutOfRangeException("Cannon index must be 0 or 1 for Player 1 and Player 2 respectively.");
                }
                else
                {
                    this.index = value;
                }
            }
        }

        public float Speed { get; set; }
        
        public float Angle { get; set; }

        public bool CanShoot { get; set; }

        public bool IsAlive { get; set; }
    }
}
