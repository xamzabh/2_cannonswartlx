﻿namespace CannonsWarTLX.Model
{
    using System.Collections.Generic;
    using CannonsWarTLX.Model.Players;

    public static class PlayfieldModel
    {
        private const float ONE_DEGREE_IN_RADIANS = 0.01745329252f;

        public static List<Player> Players { get; set; }

        public static int CurrentPlayerIndex { get; set; }

        public static void InitializePlayfieldModel()
        {
            CurrentPlayerIndex = 0;
            Players = new List<Player>()
            {
                new Player(0, -50 * ONE_DEGREE_IN_RADIANS),
                new Player(1, 50 * ONE_DEGREE_IN_RADIANS)
            };
        }
    }
}
